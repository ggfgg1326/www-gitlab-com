---
layout: markdown_page
title: "Be The Bridge Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction

On this page you will be provided an overview of our Be The Bridge Program.  This program enables those outside of GitLab to be able to be paired with a internal GitLab team member to learn more about the company, have a mentor relationship, learn more about open source, etc.
We will be working on more content on this page, if you are however interested right away, kindly complete [this interest form](https://docs.google.com/forms/d/19We1NUz5ju0ZPMIkHLivdHf60J9qJN2HizkrTAhFuS4/edit). We will respond back and hope to pair and accomodate as many of you as possible. Keep in mind we are a small company and will need to continue working on scalable solutions for awesome programs like this. 
