---
layout: markdown_page
title: "Category Direction - Dependency Scanning"
---

<!---  using https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/doc/templates/product/category_direction_template.html.md -->

- TOC
{:toc}

## Secure & Defend

| | |
| --- | --- |
| Stage | [Secure](/direction/Secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-06-22` |
| Content Last Updated | `2020-06-22` |

### Introduction and how you can help
Thanks for visiting this category direction page on Dependency Scanning at GitLab. This page belongs to the [Composition Analysis](/handbook/product/categories/#composition-analysis-group) group of the [Secure](/direction/Secure/) stage and is maintained by [Nicole Schwartz](https://gitlab.com/NicoleSchwartz).

#### Send Us Feedback
We welcome feedback, bug reports, feature requests, and community contributions.

Not sure how to get started?

- Upvote or comment on [proposed Category:Dependency Scanning issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3ADependency%20Scanning) - when you find one similar to what you want, please leave a comment AND upvote it! This helps it to be prioritized, backlog items with few unique individuals commenting are unlikely to get reviewed and prioritized.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::secure" ~"Category:Dependency Scanning" ~"group::composition analysis"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).
- Want to get directly into the user interview pool for Secure Stage? Sign up on this [google document form](https://docs.google.com/forms/d/e/1FAIpQLScCNWXVA_gBWQKB51amfmR4ccESsvZPudaEUrhQWVUZFaAf5Q/viewform?usp=sf_link).

Sharing your feedback directly on GitLab.com is the best way to contribute to our direction.

We believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Dependency Scanning is a technique that identifies project dependencies and checks if there are any known, publicly disclosed, vulnerabilities in those components that may affect the main application.

Applications define which package they require, and the version that is used. Dependency Scanning leverages our [Vulnerability Database](https://about.gitlab.com/direction/secure/vulnerability-research/vulnerability-database/) to check if any of these dependencies have known vulnerabilities, and it indicates if a package upgrade is needed.

Dependency Scanning is very dependent not only on the programming languages, but also on the package manager. Different package managers have different repositories and ways to keep track of versions.

Our goal is to provide Dependency Scanning as part of the standard development process so that we are proactively identifying potential vulnerabilities and weaknesses as they are introduced. Dependency Scanning results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown. This means that Dependency Scanning is executed every time a new commit is pushed to a branch. We also include Dependency Scanning as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

GitLab should also be able to check if a vulnerable function, when applicable, is really used by the application, and provide such information to enable better prioritization.

Dependency Scanning information can also help you create a software bill of materials (SBoM or BOM), where all the components are listed with their versions. See [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_list/) for additional details.

Dependency Scanning results are also part of the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/), where Security Teams can quickly check the security status of projects.

[Auto-remediation](https://docs.gitlab.com/ee/user/project/merge_requests/#solutions-for-dependency-scanning-ultimate) leverages Dependency Scanning to provide a solution for vulnerabilities that can be applied to fix the codebase. It will be automatically done by GitLab in the future.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
Primary: Sasha (Software Developer) wants to know when adding a depedency if it has known vulnerabilities so alternate versions or dependencies can be considered.

Secondary: Sam (Security Analyst) wants to know what dependencies have known vulnerabilities (to reduce the OWASP A9 risk - Using Components with Known Vulnerabilities), to be alerted if a new vulnerability is published for an existing component, and how behind current version the components are.

Other: Cameron (Compliance Manager), Delaney (Development Team Lead), Devon (DevOps Engineer), Sidney (Systems Administrator)

#### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
We will be researching current user challenges in [this issue](https://gitlab.com/gitlab-org/ux-research/issues/296). Please feel free to comment!

### Key features

### Strategy

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
Currently we notify developers when they add dependencies [in these supported languages](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers) with known vulnerabilities in our [vulnerability database](https://docs.gitlab.com/ee/user/application_security/index.html#maintenance-and-update-of-the-vulnerabilities-database), if [security approvals](https://docs.gitlab.com/ee/user/application_security/index.html#security-approvals-in-merge-requests) is configured we will require an approval for critical, high or unknown findings. A summary of all findings for a project can be found in the [Secuirty Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) and the [Dependency List](https://docs.gitlab.com/ee/user/application_security/dependency_list/index.html). For limited package managers, we are able to offer [auto-remediation](https://docs.gitlab.com/ee/user/application_security/index.html#solutions-for-vulnerabilities-auto-remediation) recommendations for the findings.

We know that we need to keep iterating and improving this workflow. Starting in the beginning we plan to make our setup simpler, allow customization of the approval (severity level) thresholds, and expand the languages and package managers supported for dependency discovery. Part of the language expansion will include automating population of those languages publicly disclosed findings into our vulnerability database, as well as increasing the supported package managers for auto-remediation.

Following these iterations, we need to help you assess the risk you face from a finding, GitLab should be able to indicate, where applicable, if you are using the impacted function, that has the risk. We plan to introduce tooling to help you prioritize the findings.

We also understand Security is about more than just reacting to findings, where possible we would like to warn you as components you leverage become out of date, or near end of life, to help you practively plan.

#### Roadmap
- [First MVC (already shipped)](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
- [Dependency Scanning improvements](https://gitlab.com/groups/gitlab-org/-/epics/298)

#### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
- [Support air-gapped (offline) Dependency Scanning for self-hosted instances](https://gitlab.com/groups/gitlab-org/-/epics/2011) many of our self-hosted users run in restricted networks with limited connectivity and have asked for the ability to run scans without an active internet connection.
- [Auto Remediation - User Experience for any scanner - Minimal to Viable](https://gitlab.com/groups/gitlab-org/-/epics/2460) Currently it is difficult to tell when there are suggested solutions, and we would like to reduce the work by allowing users to opt-in to having us automatically create merge requests when there is a suggested solution.
- [Expand Supported Languages and Packages](https://gitlab.com/groups/gitlab-org/-/epics/2625) - we have heard that we need to expand support to include Go and .Net. If you have additional languages you would want covered, please comment here!

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand.-- > ----->
In order to focus on what's next, the following items are not on the immidiate horizon:
- [UX improvements and research - dependency scanning](https://gitlab.com/groups/gitlab-org/-/epics/2628)
- [Customize Merge Request Security Approval threshold for Dependency Scanning](https://gitlab.com/groups/gitlab-org/-/epics/2624)
- [Help prioritize findings - Dependency Scanner](https://gitlab.com/groups/gitlab-org/-/epics/2632)
- [Proactive warnings about out of date and end of life (EOL) versions- dependency scanning](https://gitlab.com/groups/gitlab-org/-/epics/2627)
- [Notifications of prior committed dependency state (risk) change - dependency scanning](https://gitlab.com/groups/gitlab-org/-/epics/2626)
- [Dependencies approval list (allow list, deny list)](https://gitlab.com/groups/gitlab-org/-/epics/2629)

We would love your comments on any of those epics to help us prioritize the above epics as we complete the items in "What's Next & Why".

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
This category is currently at the Viable maturity level, and our next maturity target is 2021-01-31 (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)

 - [Dependency Scanning - Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/1664)

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
Currently we have very limited telemetry data, as a result we will be tracking number of times that our scans run.

In the future we hope that users will allow us to enhance our telemtry to be able to record information such as the number of findings that are dismissed vs. accepted, [and other items being discussed in this issue](https://gitlab.com/gitlab-org/secure/general/issues/61#dependency-scanning).

### Why is this important?
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
In addition to being [A9 Using Components with Known Vulnerabilities](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A9-Using_Components_with_Known_Vulnerabilities) in the OWASP top 10, keeping dependencies up to date is code quality issue, and finally as the need for software bill of materials (SBoM) grows being able to list your dependencies will become a needed feature for all application developers.

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Black Duck](https://www.blackducksoftware.com/solutions/application-security)
- [CA Veracode](https://www.veracode.com/security/)
- [Sonatype Nexus](https://www.sonatype.com/nexus-auditor)
- [Whitesource](https://www.whitesourcesoftware.com/open-source-security/)
- [greenkeeper](https://greenkeeper.io/)
- [Snyk](https://snyk.io/product/)
- [Contrast](https://www.contrastsecurity.com/open-source-security-software)

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

The Dependency Scanning topic is often coupled with License Compliance in Software Composition Analysis (SCA). This is what analysts evaluate, and how it is bundled in other products. As defined in our [Solutions](https://about.gitlab.com/handbook/product/categories/index.html#solutions), GitLab includes Container Scanning as part of Software Composition Analysis.

- [Forrester](https://www.forrester.com/report/The+Forrester+Wave+Software+Composition+Analysis+Q1+2017/-/E-RES136463)

Analysts are showing interest for auto-remediation as the key feature to make dependency scanning really actionable for users. We can invest to [increase our coverage](https://gitlab.com/groups/gitlab-org/-/epics/759).

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Scanning&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

### Top Customer Success/Sales Issue(s)

- [Issue 1](https://gitlab.com/gitlab-org/gitlab-ee/issues/6772)
- [Issue 2](https://gitlab.com/gitlab-org/gitlab-ee/issues/7115)

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name%5B%5D=customer&label_name%5B%5D=dependency%20scanning)

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->



- [Expand Supported Languages and Packages Dependency Scanning](https://gitlab.com/groups/gitlab-org/-/epics/2625)
- [Remove the Docker-in-Docker requirement for secure categories and features](https://gitlab.com/groups/gitlab-org/-/epics/2263)

I base this on upvotes as well as number of comments, so please remember to comment and/or upvote issues you would like to see.

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Scanning&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

### Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->
- [Support go in Dependency Scanning ](https://gitlab.com/gitlab-org/gitlab/issues/7132)
- [Support DS_PYTHON_VERSION variable in gemnasium-python](https://gitlab.com/gitlab-org/gitlab/issues/12848)
[Full list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=internal+customer&label_name%5B%5D=Category%3ADependency+Scanning&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)


### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
- [UX improvements and research - dependency scanning](https://gitlab.com/groups/gitlab-org/-/epics/2628)
- [Auto Remediation - User Experience for any scanner - Minimal to Viable](https://gitlab.com/groups/gitlab-org/-/epics/2460)
- [Expand Supported Languages and Packages](https://gitlab.com/groups/gitlab-org/-/epics/2625)

