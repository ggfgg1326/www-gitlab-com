---
layout: markdown_page
title: "Category Direction - Serverless"
---

- TOC
{:toc}

## Serverless

Our direction is to make GitLab the preferred tool for developers and operators looking after an integrated continuous delivery
and monitoring of serverless applications - no matter if those serverless applications are run inside a Kubernetes cluster
or with a public serverless services provider.

## North Star goal

To help our prioritization we were looking for a large-enough feature that would benefit a lot from a strong serverless backend.
Our north star is to provide support for an often requested feature, the [GitLab issue automation](https://gitlab.com/gitlab-org/gitlab/issues/35162). 

## As a first step

Leveraging Knative and Kubernetes, users will be able to define and manage functions in GitLab. This includes the security, logging, scaling, and costs of their serverless implementation for a particular project/group.

- [Viable Knative epic](https://gitlab.com/groups/gitlab-org/-/epics/1726)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/issues/99)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/155) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

We would like to strengthen our integration with existing serverless provider offerings, especially around deploying and monitoring serverless deployments.

Currently, we don't have any plans around improving the cluster based [GitLab Serveless](https://docs.gitlab.com/ee/user/project/clusters/serverless/) offering.

## Ecosystem and Partners

To get a great overview of the serverless world, we recommend having a look at the [CNCF Serverless Landscape](https://landscape.cncf.io/format=serverless).
We are working hard to be great partners with the most widely used tools and fulfill the gap in every other area.

### Knative and Kubernetes

Users should be able to easily spin a new Kubernetes cluster under various providers using GitLab to start using the GitLab serverless offering.

### Managed serverless providers

AWS Lambda is a serverless compute service created by Amazon in 2015. It runs a function triggered by an event and manages the compute resources automatically so you don’t have to worry about what is happening under the hood.

Azure Functions is Microsoft’s response to Amazon’s Lambda. It offers a very similar product for the same cost. It uses Azure Web Jobs; the delay between hot cold invocations is less visible.

It’s a fully managed Node.js environment that will run your code handling scaling, security, and performance. It’s event-driven and will trigger a function returning an event, very much in the same way AWS Lambda works. It’s intended to be used for small units of code that are placed under heavy load.

### Serverless Framework

The Serverless Framework is an open-source tool for managing and deploying serverless functions. It supports multiple programming languages and cloud providers. Its two main components are 

- [Event Gateway](https://serverless.com/event-gateway/), which provides an abstraction layer to easily design complex serverless applications, and
- [Serverless Dashboard](https://serverless.com/dashboard/), for a better management of the application, as well as collaboration.

Serverless Framework applications are written as YAML files (known as serverless.yml) which describe the functions, triggers, permissions, resources, and various plugins of the serverless application.

## Competition

We would like to provide first class solutions for monitoring, security, deployments in every major use case of serverless.

### Netlify functions

Netlify is a great tool targeting primarily frontend developers. Netlify functions is their AWS Lambda integration that
provides seemless serverless functionality for Netlify users.

### Dashbird

### Datadog

## Analyst landscape

The Serverless category is currently coupled with IaaS reports.

Gartner's `Magic Quadrant for Cloud Infrastructure as a Service` places AWS, Azure, and Google Cloud as leaders.

Forrester places `Serverless Computing` in their `Emerging Technology Spotlight` category, with the big three as leaders (AWS, Azure, Google Cloud)

## Top Customer Success/Sales issue(s)

n.a.

## Top user issue(s)

[Automatic domain for a serverless function](https://gitlab.com/gitlab-org/gitlab/issues/30151)
[SSL for Knative services](https://gitlab.com/gitlab-org/gitlab-ce/issues/56467)

## Top internal customer issue(s)

We collect [GitLab related issues under our dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/1951).

## Top Vision Item(s) 

[Serverless Vision: primary user](https://gitlab.com/gitlab-org/gitlab/issues/32543)
