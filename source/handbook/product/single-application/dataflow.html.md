---
layout: markdown_page
title: Data flow GitLab vs. multiple applications
---

How does having one application vs many applications impact workflow?

The data flows below are based on the experience of one enterprise customer who switched from multiple DevOps tools to GitLab.

The source data can be found in this [spreadsheet](https://docs.google.com/spreadsheets/d/1yui5UYY46HQ-KcwTCZ-8OXXQylBpxvEYk_QZW3qabAk/edit#gid=0).

# GitLab

```mermaid
sequenceDiagram
  Developer->>+GitLab: Login
  Developer->>+GitLab: View issue/task list
  Developer->>+GitLab: Choose an issue to fix
  Developer->>+GitLab: Change code
  Developer->>+GitLab: Submit merge request
  GitLab->>Test Environment: Build Application, Run Quality Tests, Run Security Tests, Deploy
  Test Environment->>Developer: Verify changes
  Developer->>GitLab: Deploy Application to production
  GitLab->>Production Environment: Deploy
  GitLab->>Developer: View production logs and metrics 
```


# Multiple DevOps applications

```mermaid
sequenceDiagram
  Developer->>+Issue Tracker: Login
  Developer->>+Issue Tracker: View issue/task list
  Developer->>+Issue Tracker: Choose an issue to fix
  Developer->>+Source Control: Login
  Developer->>+Source Control: Change code
  Developer->>+CI Tool: Login
  Developer->>+CI Tool: Tell CI Tool to build application
  Developer->>+CD Tool: Login
  Developer->>+CD Tool: Tell CD Tool to deploy to test environment
  CD Tool->>+Test Environment: Deploy
  Test Environment->>+Developer: Verify application
  Developer->>+Automated Test Tool: Login
  Developer->>+Automated Test Tool: Kick off automated tests
  Automated Test Tool->>+Developer: Review rest results
  Developer->>+Quality Team : Ask for quality approval
  Developer->>+SAST Tool: Login
  Developer->>+SAST Tool: Run SAST scan
  SAST Tool->>+Developer: Review results
  Developer->>Security Team: Request a DAST scan
  Security Team->>+DAST Tool: Login
  Security Team->>+DAST Tool: Run DAST scan
  DAST Tool->>+Security Team: Review results
  Security Team->>Developer: Send results
  Developer->>Dependency Scanner: Login
  Developer->>Dependency Scanner: Run dependency scan
  Dependency Scanner->>Developer: Review results
  Developer->>Secret Scanner: Login
  Developer->>Secret Scanner: Run scan for secrets
  Secret Scanner->>Developer: Review results
  Developer->>Security Team: Send all security scan results and ask for security approval
  Security Team->>Developer: Send approval
  Developer->>Production Ops: Request code push to production
  Production Ops->>CD Tool: Login
  Production Ops->>CD Tool: Tell tool to push code to production
  CD Tool->>Production Enivronment: Push code
  Production ops->>Developer: Inform of production push
  Production Environment->>Logging System: Log
  Production Environment->>Metrics System: Metrics
  Developer->>Issue Tracker: Login
  Developer->>Issue Tracker: Mark issue as fixed
  Developer->>Logging system: Login
  Logging system->>Developer: Review logs
  Developer->>Metrics system: Login
  Metrics system->>Developer: Review metrics
```



