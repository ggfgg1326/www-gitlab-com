---
layout: handbook-page-toc
title: Product Management Career Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product Management Career Development Framework
We track our progress through the skills required to be a product manager at all levels via the Product Management Career Development Framework (CDF). The single source of truth for that framework is the table below but you can use this [GoogleSheet template](https://docs.google.com/spreadsheets/d/1NaqSgu_1IcL_DYRHjrDGsKLooSykpewD3QYU2OZR5jc/edit#gid=1091464991) to track your career development with your manager. 

| IC Title                                                         | PM                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Sr. PM                                                                                                                                                                                                                                                                                                                                                                     | Principal PM                                                                                                                                                                                                                                                                                                                                                                                                             |                                                                                                                                                                                                                                                                                         |
| Manager Title                                                    |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |                                                                                                                                                                                                                                                                                                                                                                            | Group Manager PM                                                                                                                                                                                                                                                                                                                                                                                                         | Director PM                                                                                                                                                                                                                                                                             |
|------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Validation Track Skills**                                       | Adept at qualitative customer interviewing. Familiar with prioritization frameworks like RICE to organize opportunity backlogs. Capable of deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Proficient at story mapping, to break epics down into smaller [Minimal Viable Change (MVC)](/handbook/values/#minimal-viable-change-mvc) issues. Proficient at collaborating with Design on protoypes to bring potential solutions to life.                                                                                                            | Skilled at qualitative customer interviewing. Excellent at deriving key insights and patterns from customer interviews, and using that input to clarify problem statements. Skilled at story mapping, to break epics down into smaller MVC issues. Excellent at collaborating with Design on protoypes to bring potential solutions to life.                               | Skilled at qualitative customer interviewing. Skilled at story mapping, to break epics down into smaller MVC issues. Capable of teaching product validation techniques to others.                                                                                                                                                                                                                                         | Ensures consistent execution of validation track skills across a large team.                                                                                                                                                                                                             |
| **Build Track Skills**                                           | Adept at breaking epics and issues down into MVC's. Knowledgeable about GitLab's product and the relevant product domain(s). Knowledgeable about GitLab's architecture, API's, and tech stack. Capable of running a demo anytime. Able to make highly informed prioritization & tradeoff decisions with engineering. Able to discuss & evaluate technical architecture recommendations from eng. Responsible for the health of working relationships with peers in the Group. Familiar and comfortable with agile development methodologies. | Excellent at breaking epics and issues down into MVC's. Deep familiarity with GitLab's product and the relevant product domain(s). Deep understanding of GitLab's architecture and tech stack. Able to elevate performance of the Group through excellent PM outputs.                                                                                                      | Expert at breaking epics and issues down into MVC's. Expert in the relevant product domain(s) and capable of teaching others about the domain. Responsible for the health of working relationships with fellow Engineering Managers.                                                                                                                                                                                     | Ensures consistent execution of build track skills across a large team. Responsible for the health of working relationships with fellow Engineering Directors.                                                                                                                          |
| **Business Skills**                                              | Understands and communicates the business value of epics and issues. Sets success metrics for epics and issues, and tracks metrics post-launch to guide investment in iterative improvements. Spends up to 20% of time researching & defining category vision & strategy.                                                                                                                                                                                                                                                                   | Able to ensure activities are consistent with GitLab's go-to-market and business model. Can balance build, buy and partner options for solving customer problems. Can identify new market opportunities & author business cases, as well as forecast the approximate benefits of new features. Spends up to 30% of time researching & defining category vision & strategy. | Expert at business case creation. Capable of managing business results across a range of product domains.                                                                                                                                                                                                                                                                                                                | Works cross-stage and cross-functionally to ensure an excellent end-to-end customer experience. Excellent at understanding and managing the business impact across a wide range of product domains. Capable of making key pricing & packaging recommendations.                          |
| **Communication Skills**                                         | Capable written and verbal communicator internally and externally. Drives clarity in area. Trusted resource for customer calls and meetings. Builds rapport with stakeholders to align around priorities. Self aware and understands how their interactions impact others. Takes action to improve behavior based on impact to others.                                                                                                                                                                                                      | Capable of representing GitLab externally at trade shows, customer events, conferences, etc. Solid presentation skills at all levels of the company. Appropriately influences & persuade others to a course of action.                                                                                                                                                     | Recognized as a thought leader internally and externally. Excellent presentation skills at all levels of the company. Escalates issues cleanly to appropriate levels of authority when decisions or progress are blocked.                                                                                                                                                                                                | Visible leader across teams. Establishes compelling team purpose that is aligned to the overall organizational vision. Inspires broader team to achieve results. Identifies disconnects to vision and takes appropriate action.                                                         |
| **People Management Skills**                                     | N/A                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | N/A                                                                                                                                                                                                                                                                                                                                                                        | Aligns team with larger Stage vision and goals. Translates and aligns strategy in a meaningful way for team, building a shared understanding of team goals and targets. Uses situational leadership techniques to provide appropriate level of guidance and latitude to team members. Proactively identifies and fills talent gaps. Adept at caring personally for team members and providing candid real-time feedback. | Aligns team with larger Section vision and goals. Provides appropriate level of guidance and latitude to managers and individuals. Experienced at hiring and at managing out underperformance. Excellent at caring personally for team members and providing candid real-time feedback. |
| **Typical Reporting Structure**                                  | Reports to a Director or Group Manager                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | Reports to a Director or Group Manager                                                                                                                                                                                                                                                                                                                                     | Reports to a Director or VP                                                                                                                                                                                                                                                                                                                                                                                              | Reports to VP                                                                                                                                                                                                                                                                           |
| **Typical Experience**                                           | Two to four years of relevant experience or equivalent combination of experience and education.                                                                                                                                                                                                                                                                                                                                                                                                                                             | Four to six years of relevant experience or equivalent combination of experience and education.                                                                                                                                                                                                                                                                            | Eight to ten years of experience with at least one year of people management responsibility.                                                                                                                                                                                                                                                                                                                             | Ten to twelve years of experience with at least four years of people management responsibility.

### CDF Reviews
Product Management Leadership will perform CDF reviews with their team members on a regular cadence; the suggested cadence is every 2-3 months. Consider creating a recurring meeting dedicated to this topic rather than utilizing existing 1:1 meetings.

## Competencies

Product Managers must be able to demonstrate a variety of skills across our CDF. We provide additional career development guidance by listing priority [competencies](https://about.gitlab.com/handbook/competencies/) for each CDF category.

At the moment there are no defined Product Management Competencies.

## Future Competencies
Here is our prioritized list of future competencies. We'll add them to our competencies list by starting with the top priority in each skill category. We'll add more detail and content for each one of these as we add them to the PM competencies list.

Validation Track Skills
1. Performing Customer Discovery Interviews
1. Creating an Opportunity Canvas
1. Creating a Storyboard Designing Prototypes
1. Engaging Analysts via Inquiries

Build Track Skills
1. Breaking Down Your Issues 
1. Optimizing PM Inputs for Development Outputs
1. Understanding GitLab Architecture
1. Demoing GitLab

Business Skills
1. Understanding Buyer Based Tiering
1. Crafting a Strong Vision
1. Defining & Prioritizing for Success Metrics
1. Developing a Business Case
1. Investing Just Enough

Communication Skills
1. [Writing to Inspire Action](/handbook/product/communication/#writing-to-inspire-action)
1. Documenting for Clarity
1. Communicating to Executives
1. Presenting to Large Audiences
1. Representing GitLab's Entire Product Value

People Management Skills
1. Managing Team Performance
1. Facilitating Career Development Conversations
1. Coaching GitLab Values Based Product Management
1. Aligning Your Team on Strategy
1. Applying Situational Leadership

## Contributing

Every GitLab team member is encouraged to contribute to the list of prioritized Product Manager competencies, as well as the content for each competency via merge requests to this page.
