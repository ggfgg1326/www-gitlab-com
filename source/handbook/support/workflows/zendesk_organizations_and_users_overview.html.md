---
layout: handbook-page-toc
title: Zendesk Organizations Overview
category: Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Organizations

## How organizations are created?

Organizations in Zendesk are created automatically through our Salesforce and
Zendesk integration (as well as the GitLab built sync script). This integration
allows agents to see a customer's full Salesforce Profile within a live ticket
in Zendesk. You can read more about what information we send to Salesforce and
what fields are populated with information from Zendesk in the
[Support Ops handbook page](https://about.gitlab.com/handbook/support/support-ops/#salesforce---zendesk-sync).

> Please do not manually create organizations. This can break the ZD<>SFDC
> integration and cause users to receive incorrect SLAs. If you notice an
> organization needs to be created, please notify support-ops to rectify this.

## Organizations with outdated information

If you notice an organization in Zendesk contains outdated information or the
information doesn't match what Salesforce is displaying, this would indicate the
sync integration has hit an issue. Luckily, we have the GitLab built sync script
that runs every 4 hours to rectify such issues.

In your due diligence, you would want to create an issue via the 
[support-ops-project](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/issues/new)
so support-ops can double check to ensure there is nothing blocking the sync.

## Shared Organizations

You have the option of allowing all of the members in an organization to see each other's tickets. This is referred to as a shared organization. Read more about this process in the [Support Engineer Ops page](https://about.gitlab.com/handbook/support/support-ops/#shared-organizations-in-zendesk) 

## Adding users to organizations

To add a user to an organization, go to the Organization tab and on top right, click the dropdown and click on "Add User".

A form will open in which you will write the name of the user, the email of the user and the role of will be already set as "End-User". 

Make sure that the request to add a new user to an organization always comes from the Account Administrator or an official party. Always make sure to check with the Technical Account Manager or Account Owner before adding new members to an organization, especially if the users you are adding aren't using a company email.
