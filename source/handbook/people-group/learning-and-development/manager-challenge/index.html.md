---
layout: handbook-page-toc
title: Manager Challenge
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Manager Challenge page. We are rolling out a 4 week manager challenge in September 2020, "The GitLab Great Managers Program." All details relating to the challenge can be found here on this page. 

[embed overview slide deck for full challenge here] 

## Week 1

Add info for week 1 here 

[embed week 1 slide deck here] 

## Week 2

Add info for week 2 here 

[embed week 2 slide deck here] 

## Week 3

Add info for week 3 here 

[embed week 3 slide deck here] 

## Week 4

Add info for week 4 here 

[embed week 4 slide deck here] 
