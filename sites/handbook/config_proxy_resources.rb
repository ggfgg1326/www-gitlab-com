# TODO: Add this back when rest of handbook besides /marketing and /engineering is moved to sites/handbook/source
# # Hiring chart pages
# Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
#   proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
# end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true
